from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = 'article'

urlpatterns = [
    path('article/', views.article, name='article'),
    path('news/<slug:slug>/', views.news, name='news'),
    path('tipsandtrick/<slug:slug>/', views.tipsandtrick, name='tipsandtrick'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)