from django.shortcuts import render, get_object_or_404
from .models import *
from .forms import *

# Create your views here.
def article(request):
    news = News.objects.all()
    tips = TipsAndTrick.objects.all()
    context = {
        "news": news,
        "tips": tips
    }
    return render(request, "article.html", context)

def news(request, slug):
    post = get_object_or_404(News, slug=slug)
    
    if request.method == 'POST':
        comment_form = CommentNewsForm(data=request.POST)
        if comment_form.is_valid():
            comment_form.save()
            comment_form = CommentNewsForm()
    else:
        comment_form = CommentNewsForm()

    comments = post.comments_news.filter(active=True)

    context = {
        'post': post,
        'comment_form': comment_form,
        'comments': comments,        
    }
    return render(request, 'news.html', context)

def tipsandtrick(request, slug):
    post = get_object_or_404(TipsAndTrick, slug=slug)

    if request.method == 'POST':
        comment_form = CommentTipsAndTrickForm(data=request.POST)
        if comment_form.is_valid():
            comment_form.save()
            comment_form = CommentTipsAndTrickForm()
    else:
        comment_form = CommentTipsAndTrickForm()

    comments = post.comments_tips_and_trick.filter(active=True)

    context = {
        'post' : post,
        'comment_form': comment_form,
        'comments': comments,
    }
    return render(request, 'tipsandtrick.html', context)
