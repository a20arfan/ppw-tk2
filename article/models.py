from django.db import models
from django.contrib.auth.models import User
from tinymce import models as tinymce_models, HTMLField


# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=100, default="")
    slug = models.SlugField(unique=True, max_length=100, default="")
    date = models.DateField(auto_now_add=True)
    content = HTMLField('Content')
    image = models.ImageField(null=True, blank=True, upload_to='image')

    def __str__(self):
        return self.title

class TipsAndTrick(models.Model):
    title = models.CharField(max_length=100, default="")
    slug = models.SlugField(unique=True, max_length=100, default="")
    date = models.DateField(auto_now_add=True)
    content = HTMLField('Content')
    image = models.ImageField(null=True, blank=True, upload_to='image')

    def __str__(self):
        return self.title

class CommentsNews(models.Model):
    post = models.ForeignKey(News,on_delete=models.CASCADE,related_name='comments_news')
    user = models.ForeignKey(User, on_delete= models.CASCADE,related_name='comments_news_posts', default="")
    comment = models.TextField()
    published = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['published']

    def __str__(self):
        return 'Comment {} by {}'.format(self.comment, self.user)

class CommentsTipsAndTrick(models.Model):
    post = models.ForeignKey(TipsAndTrick,on_delete=models.CASCADE,related_name='comments_tips_and_trick')
    user = models.ForeignKey(User, on_delete= models.CASCADE,related_name='comments_tips_and_trick_posts', default="")
    comment = models.TextField()
    published = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['published']

    def __str__(self):
        return 'Comment {} by {}'.format(self.comment, self.user)