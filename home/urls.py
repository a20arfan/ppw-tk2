from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.Register, name='register'),
    path('', include('django.contrib.auth.urls'), name='django-auth'),
]