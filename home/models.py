from django.db import models

class Suggestions(models.Model):
	name = models.CharField(blank=False, max_length=100)
	email = models.EmailField(blank=False, max_length=50)
	suggestion = models.TextField(blank=False)

	def __str__(self):
		return self.name