from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response

from doctor.models import Doctor
import doctor.views 


#How to test: coverage run --source='doctor' manage.py test doctor
class DetailPageUnit(TestCase):
    # def test_url_detail_doctor_memiliki_HTTP_respon(self):
    #     response = Client().get(reverse('doctor:DetailDoctor', current_app=self.request.resolver_match.namespace))
    #     self.assertEqual(response.status_code, 200)
    #def test_url_detail_menggunakan_HTML_yang_sesuai(self):
    #     response = Client().get('/doctor/<id>')
    #     self.assertTemplateUsed(response, "doctor/detailDoctor.html")
    # def test_kalimat_yang_wajib_ada(self):
    #     response = Client().get('/doctor/<id>')
    #     html_kembalian = response.content.decode('utf8')
    #     self.assertIn('Known For:', html_kembalian)
    #     self.assertIn('Path to GetHealthy:', html_kembalian)
    #     self.assertIn('Passion:', html_kembalian)
    #     self.assertIn('Favourite Quote:', html_kembalian)
    def test_url_doctor_ada_url_dengan_HTTP_respon_yang_tepat(self):
        response = Client().get('/doctor/')
        self.assertEqual(response.status_code, 200)
    def test_url_doctor_menggunakan_HTML_yang_telah_ditentukan(self):
        response = Client().get('/doctor/')
        self.assertTemplateUsed(response,"doctor/listDoctor.html")
    def test_model_doctor_dapat_menyimpan_model(self):
        new_doctor = Doctor.objects.create(name='test', degree_name='test', message='test', known_for='test',
        path_to='test', passion='test', fav_quote='test', fav_quote_by='test')
        count = Doctor.objects.all().count()
        self.assertEqual(count,1)
    def test_apakah_dalam_file_html_ada_kata_atau_kalimat_yang_diwajibkan_untuk_ada(self):
        response = Client().get('/doctor/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Meet Our Doctors', html_kembalian)
        self.assertIn('Book Us!', html_kembalian)
