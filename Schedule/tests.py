from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from Schedule.models import Day, Hour, Booking
from Schedule.views import index, book
from doctor.models import Doctor

# Create your tests here.

class TestViews(TestCase):
	def setUp(self):
		self.client  = Client()
		self.doctor = Doctor.objects.create(
			name='test', 
			degree_name='test', 
			message='test', 
			known_for='test',
			path_to='test', 
			passion='test', 
			fav_quote='test', 
			fav_quote_by='test'
			)
		self.day = Day.objects.create(name="Monday", doctor=self.doctor)
		self.hour = Hour.objects.create(hour="12.00 - 13.00", day=self.day, available=True)
		self.booking = Booking.objects.create(
			orderer='dummydumdum',
			doctor=self.doctor,
			time=self.hour,
			email='dummy@email.com',
			phone='test'
			)
		self.index_url = reverse("Schedule:index")
		self.form_url = reverse('Schedule:form', args=[self.hour.id])
		
	def test_bookingpage_GET(self):
		response = self.client.get(self.index_url)
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, "Schedule/bookingpage.html")
		self.assertContains(response, "BOOK A SCHEDULE")

	def test_bookingpage_berisi_objek_day_yang_sudah_dibuat(self):
		response = self.client.get(self.index_url)
		self.assertContains(response, "Monday")
		self.assertEquals("Monday", self.hour.day.name)

	def test_bookingpage_berisi_objek_hour_yang_sudah_dibuat(self):
		response = self.client.get(self.index_url)
		self.assertContains(response, "12.00 - 13.00")

	def test_bookingform_GET(self):
		response = self.client.get(self.form_url)
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, "Schedule/bookingform.html")

	def test_bookingform_POST_buat_objek_booking(self):
		response = self.client.post(self.form_url, {"orderer" : "test", "email" : "example@test.com", "phone" : "test"})
		self.assertEquals(response.status_code, 302)
		self.assertEquals(Booking.objects.get(id=2).orderer, "test")
		self.assertEquals(Booking.objects.get(id=2).time.available, False)


class TestModels(TestCase):
	def setUp(self):
		self.doctor_dummy = Doctor.objects.create(
			name='test', 
			degree_name='test', 
			message='test', 
			known_for='test',
			path_to='test', 
			passion='test', 
			fav_quote='test', 
			fav_quote_by='test'
			)
		self.day_dummy = Day.objects.create(name="Monday", doctor=self.doctor_dummy)
		self.hour_dummy = Hour.objects.create(hour="12.00-13.00", day=self.day_dummy, available=True)
		self.booking_dummy = Booking.objects.create(
			orderer='dummydumdum',
			doctor=self.doctor_dummy,
			time=self.hour_dummy,
			email='dummy@email.com',
			phone='test'
			)

	def test_dapat_menyimpan_objek_Day(self):
		day = Day.objects.all().count()
		self.assertEquals(day, 1)

	def test_dapat_menyimpan_objek_Hour(self):
		hour = Hour.objects.all().count()
		self.assertEquals(hour, 1)

	def test_dapat_menyimpan_objek_Booking(self):
		booking = Booking.objects.all().count()
		self.assertEquals(booking, 1)

