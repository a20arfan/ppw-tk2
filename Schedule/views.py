from django.shortcuts import render, redirect
from django.views.generic import ListView
from .models import Doctor, Day, Hour, Booking
from doctor.models import Doctor
from .forms import BookingForm

# Create your views here.
def index(request):
	time = Hour.objects.all()
	schedule = {
		"Monday" : Hour.objects.filter(day__name="Monday", available=True).order_by('hour', 'day__doctor__name'),
		"Tuesday" : Hour.objects.filter(day__name="Tuesday", available=True).order_by('hour', 'day__doctor__name'),
		"Wednesday" : Hour.objects.filter(day__name="Wednesday", available=True).order_by('hour', 'day__doctor__name'),
		"Thursday" : Hour.objects.filter(day__name="Thursday", available=True).order_by('hour', 'day__doctor__name'),
		"Friday" : Hour.objects.filter(day__name="Friday", available=True).order_by('hour', 'day__doctor__name'),
	}

	context = {
		"schedule" : schedule,
	}
	
	return render(request, "Schedule/bookingpage.html", context)

def book(request, pk):
	hour = Hour.objects.get(id=pk)
	form = BookingForm()
	if request.method == 'POST':
		form = BookingForm(request.POST)
		if form.is_valid():
			orderer = form.cleaned_data['orderer']
			email = form.cleaned_data['email']
			phone = form.cleaned_data['phone']
			booking = Booking.objects.create(
				orderer=orderer,
				doctor = hour.day.doctor,
				time= hour,
				email=email,
				phone=phone
			)
			booking.save()
			hour.available = False
			hour.save()
			return redirect('Schedule:index')
	context = {
		'form' : form,
		'hour' : hour,
	}
	return render(request, 'Schedule/bookingform.html', context)
