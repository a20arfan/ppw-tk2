from django.urls import path
from . import views


app_name = 'Schedule'

urlpatterns = [
    path('', views.index, name='index'),
    path('booking_form/<int:pk>', views.book, name='form'),
    
]